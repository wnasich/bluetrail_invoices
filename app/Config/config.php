<?php
$config = [
	'App' => [
		'name' => 'Simple Invoices',
		'locale' => 'en_US.utf8',
		'defaultCurrency' => 'USD',
		'baseUrl' => '/index.php',
	],
	'AppDB' => [
		'pdo_dsn' => '',
		'pdo_user' => '',
		'pdo_pass' => '',
	],
	'TestDB' => [
		'pdo_dsn' => '',
		'pdo_user' => '',
		'pdo_pass' => '',
	],
	'TimeFormat' => [
		'date_short' => 'm/d/y',
		'date_long' => 'l jS F Y',
		'date_time_short' => 'H:i m/d/y',
		'date_time_long' => 'g:ia l jS F Y',
	],
];