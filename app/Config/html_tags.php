<?php
$config = [
	'form' => [
		't' => '<form %s>',
		'p' => [
			'action' => null,
            'method' => 'post',
            'autocomplete' => 'off',
            'class' => null,
        ]
	],
	'input' => [
		't' => '<input %s>',
		'p' => [
			'class' => 'form-control',
            'type' => 'text',
        ]
	],
	'buttonSubmit' => [
		't' => '<button type="submit" %s>' . t('Submit') . '</button>',
		'p' => [
			'class' => 'btn btn-primary',
        ]
	],
];