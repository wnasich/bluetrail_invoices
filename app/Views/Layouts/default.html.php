<!DOCTYPE html>
<html>
<head>
	<title><?= $htmlTitle ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css" media="screen">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="<?= $router->url(['controller' => 'pages', 'action' => 'index']) ?>"><?= $headTitle ?></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="<?= $router->url(['controller' => 'invoices', 'action' => 'index']) ?>"><?= t('List invoices') ?></a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container">

		<div class="row mt-3">
			<div class="col-12">
			<?php if ($viewError) { ?>
				<div class="alert alert-warning mt-2" role="alert">
					<?= $viewError ?>
				</div>
			<?php } ?>
			<?php if ($viewMessage) { ?>
				<div class="alert alert-info mt-2" role="alert">
					<?= $viewMessage ?>
				</div>
			<?php } ?>

			<?= $viewContent; ?>

			</div>
		</div>

	</div>

	<script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
	<?php if ($includeScripts) { ?>
		<?php foreach ($includeScripts as $script) { ?>
		<script src="<?= $script ?>" type="text/javascript"></script>
		<?php } ?>
	<?php } ?>
</body>
</html>
