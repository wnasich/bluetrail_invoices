<a class="btn btn-primary btn-sm float-right" href="<?= $router->url(['controller' => 'invoices', 'action' => 'index']) ?>" role="button"><?= t('Back to Invoices') ?></a>
<h4><?= t('New Invoice') ?></h4>
<hr class="my-2">
<?= $html->form(['action' => $router->url(['controller' => 'invoices', 'action' => 'add'])]) ?>
<form>
	<div class="form-row mb-2">
		<div class="col">
			<?= $html->input(['name' => 'Invoice.invoice_no', 'placeholder' => t('Invoice No')]) ?>
		</div>
		<div class="col">
			<?= $html->input(['name' => 'Invoice.sale_date', 'placeholder' => t('Date of sale')]) ?>
		</div>
		<div class="col">
			<select class="form-control" name="Invoice.payment_method">
				<option value=""><?= t('-- Pick a payment type --') ?></option>
				<?php foreach ($invoicePaymentTypes as $paymentTypeCode => $paymentTypeName) { ?>
					<?php
					$selected = (isset($formData['Invoice']['payment_method']) && $formData['Invoice']['payment_method'] == $paymentTypeCode);
					?>
					<option value="<?= $paymentTypeCode ?>" <?= $selected ? 'selected' : '' ?>><?= $paymentTypeName ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<fieldset class="mt-3">
		<label class="h5"><?= t('Customer Info'); ?></label>
		<div class="form-row mb-2">
			<div class="col">
				<?= $html->input(['name' => 'Customer.first_name', 'placeholder' => t('First name')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.last_name', 'placeholder' => t('Last name')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.address', 'placeholder' => t('Address')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.city', 'placeholder' => t('City')]) ?>
			</div>
		</div>
		<div class="form-row mb-2">
			<div class="col">
				<?= $html->input(['name' => 'Customer.postal_code', 'placeholder' => t('Postal code')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.province', 'placeholder' => t('Province')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.phone', 'placeholder' => t('Phone')]) ?>
			</div>
			<div class="col">
				<?= $html->input(['name' => 'Customer.email', 'placeholder' => t('Email')]) ?>
			</div>
		</div>
	</fieldset>

	<fieldset class="mt-3">
		<label class="h5"><?= t('Items'); ?></label>
		<div class="form-row mb-2">
			<div class="col">
				<?= t('Description') ?>
			</div>
			<div class="col">
				<?= t('Quantity') ?>
			</div>
			<div class="col">
				<?= t('Unit price') ?>
			</div>
			<div class="col">
				<?= t('Amount') ?>
			</div>
		</div>
		<?php for ($i = 0; $i < 5; $i++) { ?>
			<div class="form-row mb-2">
				<div class="col">
					<?= $html->input(['name' => "InvoiceItem.{$i}.description"]) ?>
				</div>
				<div class="col">
					<?= $html->input(['name' => "InvoiceItem.{$i}.quantity", 'rel' => 'ItemQuantity', 'type' => 'number', 'step' => '0.01']) ?>
				</div>
				<div class="col">
					<?= $html->input(['name' => "InvoiceItem.{$i}.unit_price", 'rel' => 'ItemUnitPrice', 'type' => 'number', 'step' => '0.01']) ?>
				</div>
				<div class="col">
					<?= $html->input(['rel' => 'ItemAmount', 'readonly' => true]) ?>
				</div>
			</div>
		<?php } ?>
		<div class="form-row mb-2">
			<div class="col offset-6">
				<?= t('Total') ?>
			</div>
			<div class="col">
				<?= $html->input(['rel' => 'InvoiceTotal', 'readonly' => true]) ?>
			</div>
		</div>
	</fieldset>
	<hr class="my-2">
	<div class="form-row mb-4">
		<div class="col">
		<?= $html->buttonSubmit() ?>
		</div>
	</div>
</form>