<a class="btn btn-primary btn-sm float-right" href="<?= $router->url(['controller' => 'invoices', 'action' => 'add']) ?>" role="button"><?= t('New invoice') ?></a>
<h4><?= t('Invoices') ?></h4>
<hr class="my-2">
<?php if ($invoices) { ?>
<table class="table table-hover table-bordered table-sm table-striped">
<thead>
	<tr>
		<th scope="col"><?= t('Number') ?></th>
		<th scope="col"><?= t('Issued') ?></th>
		<th scope="col"><?= t('Customer') ?></th>
		<th scope="col"><?= t('Payment') ?></th>
		<th scope="col"><?= t('Items') ?></th>
		<th scope="col" class="text-right"><?= t('Amount') ?></th>
	</tr>
</thead>
<tbody>
	<?php foreach ($invoices as $invoice) { ?>
	<tr>
		<td><?= h($invoice['invoice_no']) ?></td>
		<td><?= df($invoice['date_sale']) ?></td>
		<td><?= h($invoice['customer']) ?></td>
		<td><?= h($invoice['payment_method']) ?></td>
		<td><?= h(10) ?></td>
		<td class="text-right"><?= fc(10) ?></td>
	</tr>
	<?php } ?>
</tbody>
</table>
<?php } else { ?>
	<div class="alert alert-info mt-2" role="alert">
		<?= t('No available invoices to display') ?>
	</div>
<?php } ?>