<div class="jumbotron">
	<h1 class="display-5"><?= t('Welcome to %s', $appName) ?></h1>
	<p class="lead"><?= h('Simple webapp to help you manage your invoices') ?></p>
	<hr class="my-4">
	<a class="btn btn-primary btn-lg" href="<?= $router->url(['controller' => 'invoices', 'action' => 'index']) ?>" role="button"><?= h('List invoices') ?></a>
	<a class="btn btn-primary btn-lg" href="<?= $router->url(['controller' => 'invoices', 'action' => 'add']) ?>" role="button"><?= h('New invoice') ?></a>
</div>