<?php
/**
 * AppModel
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       App.Model
 */
namespace App\Models;

use \PDO;
use \DateTime;
use \RuntimeException;

use Libs\Connection;

class AppModel
{
    protected $connection = null;
    protected $table = null;
    protected $dateFields = [];
    protected $dateTimeFields = ['created', 'modified'];

    public function __construct()
    {
        $this->connection = Connection::getInstance();
    }

    public function getId($id)
    {
        $sql = 'SELECT * FROM ' . $this->table . ' WHERE id = :id;';

        $stm = $this->connection->prepare($sql);
        $stm->bindParam(':id', $id, PDO::PARAM_STR);
        $stm->execute();

        return $this->dataFilters($stm->fetch(PDO::FETCH_ASSOC));
    }

    public function getAllPaginated(array $params = [])
    {
        $params += [
            'page' => 0,
            'limit' => 20,
            'orderBy' => 'created DESC',
        ];

        $offset = $page * $limit;
        $sql = "SELECT * FROM {$this->table} ORDER BY {$params['orderBy']} LIMIT {$offset}, {$params['limit']};";

        $stm = $this->connection->prepare($sql);
        $stm->execute();
        $result = [];
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            $result[] = $this->dataFilters($row);
        }

        return $result;
    }

    public function dataFilters($data) {
        // Convert date string to DateTime
        foreach ($this->dateFields as $dateField) {
            if (array_key_exists($dateField, $data) && $data[$dateField]) {
                $data[$dateField] = DateTime::createFromFormat('Y-m-d|', $data[$dateField]);
            }
        }

        // Convert date string to DateTime
        foreach ($this->dateTimeFields as $dateTimeField) {
            if (array_key_exists($dateTimeField, $data) && $data[$dateTimeField]) {
                $data[$dateTimeField] = DateTime::createFromFormat('Y-m-d H:i:s', $data[$dateTimeField]);
            }
        }

        return $data;
    }

    public function save($data) {
        throw new RuntimeException('AppModel::save() not implemented');
    }
}