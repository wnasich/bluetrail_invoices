<?php
/**
 * Invoice
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       App.Model
 */
namespace App\Models;

class Invoice extends AppModel
{
    const PAYMENT_TYPE_CASH = 'cash';
    const PAYMENT_TYPE_CHECK = 'check';
    const PAYMENT_TYPE_CREDIT_CARD = 'credit_card';
    const PAYMENT_TYPE_PAYPAL = 'paypal';

    protected $table = 'invoices';
    protected $dateFields = ['sale_date'];

    private static $paymentTypeNames;

    static function paymentTypeNames()
    {
    	if (!static::$paymentTypeNames) {
    		static::$paymentTypeNames = [
    			static::PAYMENT_TYPE_CASH => t('Cash'),
    			static::PAYMENT_TYPE_CHECK => t('Check'),
    			static::PAYMENT_TYPE_CREDIT_CARD => t('Credit Card'),
    			static::PAYMENT_TYPE_PAYPAL => t('Paypal'),
    		];
    	}

    	return static::$paymentTypeNames;
    }
}