<?php

/**
 * PagesController
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       App.Controller
 */
namespace App\Controllers;

class PagesController extends AppController
{
    public function index()
    {
    	return [
    		'appName' => $this->configure->read('App.name'),
    	];
    }
}