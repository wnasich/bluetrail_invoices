<?php
/**
 * AppController
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       App.Controller
 */

namespace App\Controllers;

use \Exception;

use Libs\Configure;
use Libs\Html;
use Libs\Router;

class AppController
{
    private $viewVars = [];
    private $action = null;
    private $layout = 'Layouts' . DS . 'default';
    private $baseViewPath = APP_VIEWS_PATH;
    private $html;
    private $router;

    protected $configure;
    protected $useModels = [];

    public function __construct()
    {
        $this->configure = Configure::getInstance();
        $this->html = new Html();
        $this->router = Router::getInstance();

        foreach ($this->useModels as $model) {
            $this->loadModel($model);
        }
    }

    public function run($action = 'index', $id = 0)
    {
        if (!method_exists($this, $action)) {
            $action = 'index';
        }
        $this->action = $action;

        try {
            $this->beforeAction();
            $this->viewVars = $this->$action($id) + $this->viewVars;
        } catch (Exception $e) {
            $this->viewVars['viewError'] = 'Exception catched: ' . get_class($e) . '<br>Message: ' . $e->getMessage();
        }
        $this->render();
    }

    public function beforeAction()
    {
        $this->viewVars['htmlTitle'] = $this->configure->read('App.name');
        $this->viewVars['headTitle'] = $this->viewVars['htmlTitle'];
        $this->viewVars['viewError'] = null;
        $this->viewVars['viewMessage'] = null;
        $this->viewVars['formData'] = [];
        $this->viewVars['includeScripts'] = [];
    }

    protected function render()
    {
        extract($this->viewVars);
        $this->html->formData = $this->viewVars['formData'];

        // Allow access to helper classes from view files
        $router = $this->router;
        $html = $this->html;

        // Render view content
        ob_start();
        $controllerBaseName = str_replace('App\\Controllers\\', '', get_called_class());
        $controllerBaseName = str_replace('Controller', '', $controllerBaseName);
        include $this->baseViewPath . DS . $controllerBaseName . DS . $this->action . '.html.php';
        $viewContent = ob_get_clean();

        // Render main layout
        include $this->baseViewPath . DS . $this->layout . '.html.php';
    }

    public function loadModel($model)
    {
        $modelName = str_replace('App\\Models\\', '', $model);
        $this->{$modelName} = new $model();
    }

    public function paginationParams()
    {
        $maxLimit = 200;
        $params = [];
        if (isset($_GET['page'])) {
            $params['page'] = intval($_GET['page']);
        }
        if (isset($_GET['limit'])) {
            $params['limit'] = intval($_GET['limit']);
        }
        if ($params['limit'] > $maxLimit) {
            $params['limit'] = $maxLimit;
        }

        return $params;
    }

    public function redirect($url)
    {
        $toUrl = $this->router($url);
        header('Location: ' . $toUrl);
        exit;
    }
}