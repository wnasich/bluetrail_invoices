<?php

/**
 * InvoicesController
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       App.Controller
 */
namespace App\Controllers;

use App\Models\Invoice;

class InvoicesController extends AppController
{
	protected $useModels = [Invoice::class];

    public function index()
    {
    	return [
    		'invoices' => $this->Invoice->getAllPaginated($this->paginationParams()),
    	];
    }

    public function add()
    {

    	$formData = [];
    	if (!empty($_POST)) {
    		if ($this->Invoice->save($formData)) {
    			$this->redirect(['controller' => 'invoices', 'action' => 'index']);
    		}
    	}

    	return [
    		'formData' => $formData,
    		'invoicePaymentTypes' => Invoice::paymentTypeNames(),
    		'includeScripts' => ['js/views/invoices/add.js'],
    	];
    }
}