<?php
/**
 * Html
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       Libs
 */
namespace Libs;

use \DateTime;
use \RuntimeException;
use \InvalidArgumentException;

class Html
{
    public $tags = [];
    public $formData = [];

    public function __construct()
    {
        $configFilePath = APP_CONFIG_PATH . DS . 'html_tags.php';
        if (!file_exists($configFilePath)) {
            throw new RuntimeException('Configuration file ' . $configFilePath . ' not found');
        }

        require $configFilePath;
        $this->tags = $config;
    }

    public function __call($method, $params)
    {
        return $this->expandTag($method, ($params ? $params[0] : []));
    }

    public function expandTag($tag, array $tagParams = [])
    {
        if (!array_key_exists($tag, $this->tags)) {
            throw new InvalidArgumentException("Tag {$tag} not found in property 'tags'");
        }
        $tagParams += $this->tags[$tag]['p'];

        return sprintf($this->tags[$tag]['t'], $this->expandAttributes($tagParams));
    }

    public function expandAttributes($attributeSet = [])
    {
        $result = [];
        foreach ($attributeSet as $attribute => $value) {
            if ($value) {
                $result[] = $attribute . '="' . $value . '"';
            }
        }

        return join(' ', $result);
    }

    public function input(array $tagParams = [])
    {
        $model = null;
        $field = null;
        if (!empty($tagParams['name'])) {
            $nameComponents = explode('.', $tagParams['name']);
            if (count($nameComponents) == 2) {
                list($model, $field) = $nameComponents;
            }
        }

        if ($model && $field && isset($this->formData[$model][$field])) {
            $value = $this->formData[$model][$field];
            if ($value instanceof DateTime) {
                $value = $value->format('Y-m-d');
                $tagParams['type'] = 'date';
            }

            $tagParams['value'] = $value;
        }

        return $this->expandTag('input', $tagParams);;
    }
}