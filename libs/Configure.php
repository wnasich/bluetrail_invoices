<?php
/**
 * Configure
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       Libs
 */
namespace Libs;

use \RuntimeException;
use \InvalidArgumentException;

class Configure
{
    private static $instance = null;
    private static $config = [];
    private static $basePath = APP_CONFIG_PATH;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    public static function load($configFile) {
        $configFilePath = static::$basePath . DS . $configFile . '.php';
        if (!file_exists($configFilePath)) {
            throw new RuntimeException('Configuration file ' . $configFilePath . ' not found');
        }

        require $configFilePath;

        foreach ($config as $key1 => $value1) {
            if (is_array($value1)) {
                foreach ($value1 as $key2 => $value2) {
                    static::write($key1 . '.' . $key2, $value2);
                }
            } else {
                static::write($key1, $value1);
            }
        }
    }

    public static function read($key = null) {
        $keys = explode('.', $key);
        $count = count($keys);

        switch ($count) {
            case 0:
                return static::$config;
            break;
            case 1:
                return static::$config[$keys[0]];
            break;
            case 2:
                return static::$config[$keys[0]][$keys[1]];
            break;
            default:
                return null;
            break;
        }
    }

    public static function write($key = null, $value) {
        $keys = explode('.', $key);
        $count = count($keys);

        if (!$key) {
            throw new InvalidArgumentException('Parameter key can not be empty');
        }

        switch ($count) {
            case 1:
                static::$config[$keys[0]] = $value;
            break;
            case 2:
                static::$config[$keys[0]][$keys[1]] = $value;
            break;
            default:
                throw new InvalidArgumentException('Parameter key does not support more than 2 levels');
            break;
        }
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}