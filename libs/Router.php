<?php
/**
 * Router
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       Libs
 */
namespace Libs;

use \RuntimeException;
use \InvalidArgumentException;
use \Libs\Configure;

class Router
{
    private static $instance = null;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    public static function url(array $path = []) {
        $url = Configure::read('App.baseUrl');
        $queryParams = [];
        foreach ($path as $key => $value) {
            switch ($key) {
                case 'controller':
                    $queryParams['m'] = $value;
                break;
                case 'action':
                    $queryParams['a'] = $value;
                break;
                default:
                    $queryParams[$key] = $value;
                break;
            }
        }
        $url .= '?' . http_build_query($queryParams);

        return $url;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}