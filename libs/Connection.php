<?php
/**
 * Connection
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 * @package       Libs
 */
namespace Libs;

use \PDO;

class Connection
{
    private static $instance = null;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $dbConfig = Configure::read(DB_CONNECTION_CONFIG);
            $instance = new PDO($dbConfig['pdo_dsn'], $dbConfig['pdo_user'], $dbConfig['pdo_pass']);
        }

        return $instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}