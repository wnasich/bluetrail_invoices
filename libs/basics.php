<?php
if (!function_exists('h')) {
	// Convenient alias for escaping special chars in outputs
	function h($text)
	{
		return htmlspecialchars($text);
	}
}

if (!function_exists('t')) {
	// Translation of string to user's language
	// Not implemented
	function t($text, $args = null)
	{
		$arguments = func_get_args();
		return h(sprintf($text, array_slice($arguments, 1)));
	}
}


if (!function_exists('df')) {
	function df($date, $format = null)
	{
		if (!$format) {
			$format = \Libs\Configure::read('TimeFormats.date_short');
		}
		return $date ? $date->format($format) : '';
	}
}

if (!function_exists('dtf')) {
	function dtf($dateTime, $format = null)
	{
		if (!$format) {
			$format = \Libs\Configure::read('TimeFormats.date_time_short');
		}
		return $dateTime ? $dateTime->format($format) : '';
	}
}

if (!function_exists('fc')) {
	function fc($amount, $currency = null)
	{
		if (!$currency) {
			$currency = \Libs\Configure::read('App.defaultCurrency');
		}
		$locale = \Libs\Configure::read('App.locale');
		$formatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);
		return $amount ? $formatter->formatCurrency($amount, $currency) : '';
	}
}