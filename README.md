BlueTrail Homework: Invoices
=======================================

## Download and setting application

```
$ git clone git@bitbucket.org:wnasich/bluetrail_invoices.git
$ cp app/Config/config.php app/Config/config-local.php
```
Edit `app/Config/config-local.php` and update entry 'AppDB' with your DB info

## Create DB scheme
Use file provided on app/Config/schema.sql

### Locales
Configure required locales
```
$ sudo locale-gen en_US.utf8
```

## Execution

Configure your web server to serve webroot/index.php.

You can test it using internal PHP server running on project folder:
```
$ php -S 0.0.0.0:8089 -t webroot
```

