;(function($) {
	$(document).ready(function() {
		var $invoiceTotal = $('input[rel="InvoiceTotal"]');

		$('input[rel="ItemQuantity"], input[rel="ItemUnitPrice"]').change(function (e) {
			var $itemAmount = $(this).closest('.form-row').find('input[rel="ItemAmount"]');
			updateItemAmount($itemAmount);
		});

		function updateItemAmount($itemAmount) {
			var $itemQuantity = $itemAmount.closest('.form-row').find('input[rel="ItemQuantity"]'),
				$itemUnitPrice = $itemAmount.closest('.form-row').find('input[rel="ItemUnitPrice"]'),
				quantity = 0,
				unitPrice = 0;

			if ($itemQuantity.length > 0) {
				quantity = parseFloat($itemQuantity.val()) || 0;
			}
			if ($itemUnitPrice.length > 0) {
				unitPrice = parseFloat($itemUnitPrice.val()) || 0;
			}
			if ($itemAmount.length > 0) {
				$itemAmount.val(Math.round(quantity * unitPrice * 100) / 100);
			}

			updateInvoiceTotal();
		}

		function updateInvoiceTotal() {
			var total = 0;

			$('input[rel="ItemAmount"]').each(function (index, element) {
				total += parseFloat($(element).val()) || 0;
			});

			$invoiceTotal.val(total);
		}

		updateInvoiceTotal();

	});
})(jQuery);
