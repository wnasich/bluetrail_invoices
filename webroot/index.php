<?php
/**
 * Invoice webapp entry point
 *
 * Blue Trail Homework: PHP Invoice Challenge
 * https://gist.github.com/iturricf/52f1bc87fd785536ed7bf3f52733274a
 *
 * June 2020
 * @author        Walter Nasich - wnasich@gmail.com
 */
use Libs\Configure;
use App\Controllers\PagesController;
use App\Controllers\InvoicesController;

define('DB_CONNECTION_CONFIG', 'AppDB');
define('DS', DIRECTORY_SEPARATOR);
define('APP_CONFIG_PATH', '..' . DS . 'app' . DS . 'Config');
define('APP_VIEWS_PATH', '..' . DS . 'app' . DS . 'Views');

spl_autoload_register(function($class) {
	$path = explode('\\', $class);
	$path[0] = strtolower($path[0]);
	$classFile = '..' . DS . join(DS, $path) . '.php';;
    include $classFile;
});

include '..' . DS . 'libs' . DS . 'basics.php';

$configure = Configure::getInstance();

$configure->load('config');
$configure->load('config-local');

setlocale(LC_ALL, $configure->read('App.locale'));


$module = isset($_GET['m']) ? $_GET['m'] : '';
$action = isset($_GET['a']) ? $_GET['a'] : '';
$id = isset($_GET['id']) ? $_GET['id'] : '';

switch($module) {
    case 'invoices':
        $controller = new InvoicesController();
        break;
    default:
        $controller = new PagesController();
}

$controller->run($action, $id);